Questions:

 - If I wanted the temperature in Kelvin rather than celcius, how could I specify this in API calls?
  I'd use a parameter in querystring, as this could be used both in url like
  http://carminatimarco.pythonanywhere.com/weather/london/20171117/0900/
  http://carminatimarco.pythonanywhere.com/weather/london/20171117/0900/temperature

 - How would you test this REST service?
  I tested the REST service using unittest, creating differents TestCase (see test_weather.py).

 - How would you check the code coverage of your tests?
  I installed in dev the library coverage and running
  coverage run test_weather.py
  coverage report -m

  I obtained
  api.py                             40      2    95%   51, 64
  constants.py                        5      0   100%
  test_weather.py                    38      0   100%

  So I know what are the lines to check (ie. 51 and 64 to check)

 - How could the API be documented for third-parties to use?
    I usually write the documentation with Confluence, https://confluence.atlassian.com/doc/develop-technical-documentation-in-confluence-226166494.html
    It's easy to use and it creates good documentation with few effort.

    But, we can use sphinx http://www.sphinx-doc.org/en/stable/ or
    dexy http://www.dexy.it/docs/getting-started.html#simple-dexy-example


 - How would you restrict access to this API?
    To resolve many connections I'd set a connection limit in load balancer.
    To restrict access to a single person, I'd create API_KEY/token to limit the no. of calls.

 - What would you suggest is needed to do daily forecast recoveries from openweather.org, keeping the web service up to date?
    I'd create a new url, for example '/weather/london/today/' and
    I'd obtain the next valid value for hours and day,
    making operation using datetime library; In fact, you need to calculate
    both the day and the time, as if it's 10 am and you
    make an API call using 9 am as time, you'll not receive the result.

Personal Note:    
  'london', that you can find in the url, could be a parameter to search to
  substitute the "q=London,uk" that is in the WS url

Deliverables:

 * A github/bitbucket/Other git repository that can be cloned.

it's https://bitbucket.org/carminati_marco/flask.rest.test

 * Instructions for how to set up and get the service running.

 it's in the README.md on project

Bonus Points (Optional):

 * A service running somewhere public I can curl.

 I intalled the project in http://carminatimarco.pythonanywhere.com/

 * A vagrant environment / docker container(s) to run the service and all its tests.
