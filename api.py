from datetime import datetime
import requests
from flask import Flask
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

from constants import INDIVIDUAL_LIST, INDIVIDUAL_DICT, WS_URI, TEMP, KELVIN_CELSIUS

class BaseAPI(Resource):
    def get(self):
        return {'hello': 'it works!'}

def _cast_datetime(weather_date, weather_hour_minute):
    """ Obtains date and hour in strings from received format to needed format.
    """
    try:
    	string_date = '%(weather_date)s %(weather_hour_minute)s' % \
        dict(weather_date=weather_date, weather_hour_minute=weather_hour_minute)
    	return datetime.strptime(string_date, "%Y%m%d %H%M%S").strftime("%Y-%m-%d %H:%M:%S")

    except ValueError:
      	return None

def _cast_result(result):
    """ Check if any node should be casted
    """
    #: if there is 'TEMP' in result, it'll change K in C and add 'C'.
    if TEMP in result:
        result[TEMP] = str(int(result[TEMP] - KELVIN_CELSIUS)) + 'C'

    return result

class WeatherAPI(Resource):
    """ Resource Weather.
    """
    def get(self, weather_date, weather_hour_minute, weather_individual=None):

        #: create the datetime.
        dt_txt = _cast_datetime(weather_date, weather_hour_minute)

        if not dt_txt: #: if dt_txt is None, return data due ValueError
            result = dict(status= "error", message="No data for %(weather_date)s %(weather_hour_minute)s" \
            % dict(weather_date=weather_date, weather_hour_minute=weather_hour_minute))
        else:
            # call the uri and search the dict.
            r = requests.get(WS_URI)
            output_dict = [x for x in r.json()['list'] if x['dt_txt'] == dt_txt]
            if len(output_dict) == 1:
                # it found the value, it can create the result.
                main = output_dict[0]['main']
                if weather_individual:
                    result = { weather_individual : main[INDIVIDUAL_DICT[weather_individual]] }
                else:
                    result = dict({value: main[INDIVIDUAL_DICT[value]] for value in INDIVIDUAL_LIST })
                    #: add description in dict (i'm in NO weather_individual section).
                    result['description'] = output_dict[0]['weather'][0]['description']
            else:
                result = dict(status= "error", message=("No data for %s" % dt_txt))

        r = _cast_result(result)
        #print r
        return r


api.add_resource(BaseAPI, '/')
api.add_resource(WeatherAPI,  '/weather/london/<weather_date>/<weather_hour_minute>/',
'/weather/london/<weather_date>/<weather_hour_minute>/<weather_individual>' )


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
