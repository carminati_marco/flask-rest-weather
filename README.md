# README #

You can find the API working here
http://carminatimarco.pythonanywhere.com/

## How do I get set up? ##
After cloning the repository

```
git clone https://carminati_marco@bitbucket.org/carminati_marco/flask.rest.test.git
```

### Docker it ###
```
docker build -t flask-rest-test:latest .
docker run -d -p 5000:5000 flask-rest-test
```

### or install in your machine ###

* Install and start

create a new Virtualenv [(install virtualenv)](https://virtualenv.pypa.io/en/stable/installation/)

```
mkvirtualenv flask_rest --python=/usr/bin/python2.7
```

install the required library
```
pip install -r requirements.txt
```

and start the service
```
python api.py
```

* Test

After creating the virtualenv, you can test the services with the follow command
```
python test_weather.py
```

### Who do I talk to? ###

* carminati.marco@gmail.com
