import unittest
import os
import json
import api
from datetime import date, timedelta

class WeatherTestCase(unittest.TestCase):
    """This class represents test case"""

    def setUp(self):
        """Define test variables and initialize app."""
        api.app.testing = True
        self.client = api.app.test_client

    def test_api_root(self):
        """Test API root"""
        res = self.client().get('/')
        data = json.loads(res.get_data(as_text=True))
        self.assertEqual(res.status_code, 200)
        self.assertIn('it works', data['hello'])

    def test_api_general_summary(self):
        """Test API general_summary"""
        #: create url.
        weather_date = (date.today() + timedelta(days=1)).strftime("%Y%m%d")
        url = '/weather/london/%(weather_date)s/0900/' % dict(weather_date=weather_date)
        res = self.client().get(url)
        data = json.loads(res.get_data(as_text=True))
        print data
        self.assertEqual(res.status_code, 200)

    def test_api_general_individual(self):
        """Test API general_individual"""
        #: create url.
        weather_date = (date.today() + timedelta(days=1)).strftime("%Y%m%d")
        individual_list = api.INDIVIDUAL_LIST
        for individual in individual_list:
            #: create url.
            url = '/weather/london/%(weather_date)s/0900/%(individual)s' % dict(weather_date=weather_date, individual=individual)
            res = self.client().get(url)
            data = json.loads(res.get_data(as_text=True))
            print data
            self.assertEqual(res.status_code, 200)
            self.assertTrue(individual in data)


    def test_api_general_no_data(self):
        """Test API to obtain BAD status"""
        res = self.client().get('/weather/london/17670812/0900/temperature')
        data = json.loads(res.get_data(as_text=True))
        self.assertEqual(res.status_code, 200)
        self.assertIn('error', data['status'])


if __name__ == "__main__":
    unittest.main()
