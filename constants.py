
INDIVIDUAL_LIST = ['temperature', 'pressure', 'humidity']
INDIVIDUAL_DICT = dict(temperature='temp', pressure='pressure', humidity="humidity")

TEMP = 'temperature'
KELVIN_CELSIUS = 273.15

WS_URI = 'http://api.openweathermap.org/data/2.5/forecast?q=London,uk&APPID=f89967c8b8ff0e1b36c8d4a906b2f974'
