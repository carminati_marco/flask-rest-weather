Create a python/flask REST web service providing an API to query the data.



The attached London weather forecast.json comes from www.openweathermap.org. The data format reference ishttp://www.openweathermap.org/forecast5 and can be recovered via API call http://api.openweathermap.org/data/2.5/forecast?q=London,uk&APPID={appid} (free registration required).



I would like to make the following calls against this web service using "curl":



# A general summary of the weather:

#

curl http://<host:ip>/weather/london/<date>/<hour minute>/



# Note, temperature converted from Kelvin to C and rounded up.

e.g. curl http://<host:ip>/weather/london/20160706/0900/

{

  "description": "few clouds",

  "temperature": "15C",

  "pressure": "1028.12",

  "humidity": "88%"

}



# I would like to be able to ask for individual pieces of information:

#

curl http://<host:ip>/weather/london/<date>/<hour minute>/temperature

e.g. curl http://<host:ip>/weather/london/20160706/0900/temperature/

{

  "temperature": "15C"

}



curl http://<host:ip>/weather/london/<date>/<hour minute>/pressure

e.g. curl http://<host:ip>/weather/london/20160706/0900/pressure/

{

  "pressure": "1028.12"

}



curl http://<host:ip>/weather/london/<date>/<hour minute>/humidity

e.g. curl http://<host:ip>/weather/london/20160706/0900/humidity/

{

  "humidity": "88%"

}



# When no data is found I would like see the response:

#

curl http://<host:ip>/weather/london/17670812/0900/temperature

{

  "status": "error", "message": "No data for 1767-08-12 09:00"

}


# Questions: see QA.md



Questions:

 - If I wanted the temperature in Kelvin rather than celcius, how could I specify this in API calls?

 - How would you test this REST service?

 - How would you check the code coverage of your tests?

 - How could the API be documented for third-parties to use?

 - How would you restrict access to this API?

 - What would you suggest is needed to do daily forecast recoveries from openweather.org, keeping the web service up to date?





Deliverables:

 * A github/bitbucket/Other git repository that can be cloned.

 * Instructions for how to set up and get the service running.





Bonus Points (Optional):

 * A service running somewhere public I can curl.

 * A vagrant environment / docker container(s) to run the service and all its tests.
